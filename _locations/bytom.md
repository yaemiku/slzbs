---
title: Bytom
---

Zajęcia z brydża sportowego dla dzieci i młodzieży od podstaw i nie tylko
w Młodzieżowym Domu Kultury Nr1 w Bytomiu ul.Powstańców Warszawskich 12

Piątek 15.30 - 19.00
Sobota 9.30 - 13.00

Informacje dodatkowe: Jerzy Matura tel. 507 122 884
