---
title: Bielsko-Biała
---

Zajęcia grupa początkująca 1 wtorek 13.40 - 15.10  
Zajęcia grupa początkująca 2 wtorek 15.15 - 16.45  
V LO Bielsko-Biała Adres: Józefa Lompy 10, 43-300 Bielsko-Biała

Turniej wraz z seniorami z UIIIW ATH Bielsko-Biała  
Piątek 16.00 - 19.30  
Klub Osiedlowy "Karpatek"  
ul. Matusiaka 3  
43-316 Bielsko-Biała
