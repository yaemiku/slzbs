---
title: Uczniowski Klub ZSP Pszczyna
---

Koło brydżowe działa od 2019 roku w Zespole Szkolno- Przedszkolnym w Pszczynie. Aktualnie zajęcia odbywają się dwa razy w tygodniu hybrydowo, czyli jeden dzień stacjonarnie w szkole, a drugi online na platformach brydżowych. Koło liczy 10 członków, są to dzieci z roczników 2010-2011. Wkrótce planujemy powiększyć grupę o dzieciaki z roczników 2012-2014. Prowadzący Dawid Ochman.

[Zawodnicy](https://msc.com.pl/cezar/?p=209&action=1&id=11760)
