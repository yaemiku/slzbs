---
title: SBS Blue Club Chorzów
---

Stwarzyszenie Brydża Sportowego Blue Club Chorzów organizuje zajęcia dydaktyczno – sportowe, obozy wypoczynkowo-sportowe, wyjazdy weekendowe na turnieje sportowe dla dzieci i młodzieży z Chorzowa i okolic.  
Naszym celem jest rozwój i integracja uczniów szkół podstawowych i średnich poprzez popularyzację gry w brydża sportowego.  
Budując most (BRIDGE) między uczniami z różnych szkół i w różnym wieku stwarzamy możliwość rozwoju na wielu płaszczyznach: intelektualnej, towarzyskiej, społecznej.  
Spotykamy się regularnie w każdy piątek o godzinie 17.00 w Słowaku – I LO w Chorzowie ul. Dąbrowskiego. W tygodniu ćwiczymy online na platformie brydżowej. Uczestniczymy w wielu imprezach: mistrzostwach, mityngach, obozach sportowych.

[Zawodnicy](https://msc.com.pl/cezar/?p=209&action=1&id=11567)
