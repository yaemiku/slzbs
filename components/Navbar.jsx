import Route from "@components/Route";
import { routes } from "./Routes";

export default function Header() {
  return (
    <nav className="max-w-3xl">
      <ul className="flex flex-wrap gap-x-8 gap-y-2 justify-center">
        {routes.map(({ title, href, match }, index) => (
          <Route
            title={title}
            href={href}
            match={!!match ? match : href}
            key={index}
          />
        ))}
      </ul>
    </nav>
  );
}
