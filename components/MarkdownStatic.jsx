import { serialize } from "next-mdx-remote/serialize";
import fs from "fs";
import path from "path";
import matter from "gray-matter";

export const markdownProps =
  (dir) =>
  async ({ params: { slug } }) => {
    const markdownWithMeta = fs.readFileSync(
      path.join(dir, slug + ".md"),
      "utf-8"
    );

    const { data: frontMatter, content } = matter(markdownWithMeta);
    const mdxSource = await serialize(content);

    return {
      props: {
        frontMatter,
        slug,
        mdxSource,
      },
    };
  };

export const markdownPaths = (dir) => async () => ({
  paths: fs.readdirSync(path.join(dir)).map((filename) => ({
    params: {
      slug: filename.replace(".md", ""),
    },
  })),
  fallback: false,
});

export const markdownsList = (dir) =>
  Promise.all(
    fs.readdirSync(path.join(dir)).map(async (filename) => {
      const markdownWithMeta = fs.readFileSync(
        path.join(dir, filename),
        "utf-8"
      );

      const { data: frontMatter, content } = matter(markdownWithMeta);
      const mdxSource = await serialize(content);

      return {
        frontMatter,
        mdxSource,
        slug: filename.split(".")[0],
      };
    })
  );

export const markdownsProps =
  (dir, sort = () => true) =>
  async () => ({
    props: {
      markdowns: (await markdownsList(dir)).sort(sort),
    },
  });

const strCompare = (a, b) => a?.localeCompare(b);

export const sortByDate = (a, b) => b.frontMatter.date - a.frontMatter.date;
export const sortByFilename = (a, b) => strCompare(a.filename, b.filename);
export const sortByTitle = (a, b) =>
  strCompare(a.frontMatter.title, b.frontMatter.title);
