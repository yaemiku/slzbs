import Head from "next/head";
import Header from "@components/Header";
import Navbar from "@components/Navbar";
import Footer from "@components/Footer";

export default function Layout({ children }) {
  return (
    <div className="h-full container mx-auto py-6 px-4 md:px-0 flex flex-col items-center gap-8">
      <Head>
        <title>Śląski Związek Brydża Sportowego - Młodzież</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />
      <Navbar />

      <main className="prose prose-slate dark:prose-invert prose-h1:text-center prose-h1:mt-10">
        {children}
      </main>

      <Footer />
    </div>
  );
}
