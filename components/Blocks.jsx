import Link from "next/link";

export default function Blocks({ children, href }) {
  return (
    <div className="flex flex-wrap justify-center gap-4 gap-x-4 mb-8 not-prose">
      {children.map((post, index) => (
        <Link href={`/${href}/${post.slug}`} key={index} passHref>
          <a
            className={
              (post.frontMatter.color ||
                "bg-slate-100 dark:bg-slate-800 border-slate-300 dark:border-slate-700") +
              " border rounded-md shadow-md cursor-pointer p-4"
            }
          >
            <p className="font-bold text-center">{post.frontMatter.title}</p>
            <h3 className="text-center">{post.frontMatter.description}</h3>
            <p className="font-light text-center">
              {post.frontMatter.date?.toLocaleString("pl-PL")}
            </p>
          </a>
        </Link>
      ))}
    </div>
  );
}
