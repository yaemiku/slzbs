import Link from "next/link";
import { useRouter } from "next/router";

export default function Router({ title, href, match }) {
  const router = useRouter();
  const route = `/${router.pathname.split("?")[0].split("/")[1]}`;

  const compare = (a, ...b) => b.includes(a);

  return (
    <li
      className={
        "dark:text-slate-200" +
        (compare(route, href, match) ? " font-bold" : "")
      }
    >
      <Link href={href}>{title}</Link>
    </li>
  );
}
