import { MDXRemote } from "next-mdx-remote";
import { useRouter } from "next/router";
import Link from "next/link";
import { goBack } from "@components/Routes";
import Blocks from "@components/Blocks";

export const Markdown = ({ frontMatter: { title, date }, mdxSource }) => {
  const route = useRouter().asPath;

  return (
    <div className="mt-4">
      <h2 className="mb-0">{title}</h2>
      <h6 className="font-light">{date?.toLocaleString("pl-PL")}</h6>
      <Link href={goBack(route)} passHref>
        <a className="italic">powrót</a>
      </Link>
      <MDXRemote {...mdxSource} />
    </div>
  );
};

export const Markdowns =
  (heading, href) =>
  ({ markdowns }) =>
    (
      <>
        <h1 className="text-center">
          <p>{heading}</p>
        </h1>
        <Blocks href={href}>{markdowns}</Blocks>
      </>
    );
