import Link from "next/link";

export default function Header() {
  return (
    <header className="flex items-center text-center gap-x-4 text-4xl dark:text-slate-50">
      <img src="/orzel.webp" className="max-h-16" alt="" />
      <Link href="/">Śląski Związek Brydża Sportowego - Młodzież</Link>
      <img src="/orzel.webp" className="max-h-16" alt="" />
    </header>
  );
}
