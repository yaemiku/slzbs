export const routes = [
  { title: "Strona główna", href: "/", match: "/impreza" },
  { title: "Kalendarz", href: "/kalendarz" },
  { title: "Gdzie gramy?", href: "/gdzie", match: "/rozgrywki" },
  { title: "Wyniki", href: "/wyniki" },
  { title: "Kluby", href: "/kluby", match: "/klub" },
  { title: "Sukcesy", href: "/sukcesy" },
  { title: "System", href: "/system" },
  { title: "Baza rozdań", href: "/rozdania" },
  { title: "Galeria", href: "/galeria" },
  { title: "Archiwum", href: "https://www.pzbs.pl/wyniki-mlodziezowe" },
  { title: "Linki", href: "/linki" },
  { title: "ABC Brydża", href: "/abc" },
  { title: "Do pobrania", href: "/pliki" },
  { title: "Kontakt", href: "/kontakt" },
  { title: "Dlaczego warto grać w brydża?", href: "/dlaczego" },
];

export const goBack = (path) =>
  routes.find(({ match }) => path.includes(match))?.href;
