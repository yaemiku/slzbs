export default function Footer() {
  const year = new Date().getFullYear();
  return (
    <footer className="mt-auto font-light pb-4 dark:text-slate-50">
      &copy; {year} <span className="font-medium">Mikołaj Kubiczek</span>
    </footer>
  );
}
