import Blocks from "@components/Blocks";
import { MDXRemote } from "next-mdx-remote";
import Link from "next/link";
import {
  markdownsList,
  sortByDate,
  sortByFilename,
} from "@components/MarkdownStatic";

const Home = ({ events, posts }) => {
  return (
    <>
      <Blocks href="impreza">{events}</Blocks>
      <h1 className="text-center mb-0">
        <p>Aktualności</p>
      </h1>
      <div className="flex flex-col">
        {posts.slice(0, 5).map((post, index) => (
          <div
            className={
              "pb-4 " +
              (posts.length > 5
                ? "border-b-2 border-dashed"
                : index != 0
                ? "border-t-2 border-dashed"
                : "")
            }
            key={index}
          >
            <h2>{post.frontMatter.title}</h2>
            <h6 className="font-light">
              {post.frontMatter.date.toLocaleString("pl-PL")}
            </h6>
            <MDXRemote {...post.mdxSource} />
          </div>
        ))}
      </div>
      {posts.length > 5 ? (
        <h3 className="text-center mb-0">
          <p>
            Więcej postów w <Link href="/posty">archiwum</Link>
          </p>
        </h3>
      ) : null}
    </>
  );
};

export const getStaticProps = async () => {
  const events = (await markdownsList("_events")).sort(sortByFilename);
  const posts = (await markdownsList("_posts")).sort(sortByDate);

  return {
    props: {
      events,
      posts,
    },
  };
};

export default Home;
