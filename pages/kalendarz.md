# Kalendarz

Kalendarz zawodów brydżowych w ramach sportu młodzieżowego w roku 2022:

| Wydarzenie                                                | Data                                                   |
| --------------------------------------------------------- | ------------------------------------------------------ |
| Mistrzostwa Polski Dzieci                                 | **8-10.04.2022**                                       |
| Eliminacje OOM                                            | **19.04.2022**                                         |
| Międzywojewódzkie Mistrzostwa Młodzików (edycja wiosenna) | **23.04.2022**                                         |
| Ogólnopolska Olimpiada Młodzieży                          | **24-28.05.2022**                                      |
| Mistrzostwa Polski Młodzieży Szkolnej                     | **16-19.06.2022**                                      |
| BOOM Turnus 1                                             | **29.07-8.08.2022**                                    |
| BOOM Turnus 2                                             | **8-18.08.2022**                                       |
| BOOM Turnus 3                                             | **18-28.08.2022**                                      |
| Międzywojewódzkie Mistrzostwa Młodzików (edycja jesienna) | **09.09.2022 do 30.11.2022 (do ustalenia w strefach)** |
| Mistrzostwa Polski Młodzików                              | **06-09.10.2022**                                      |
| Mistrzostwa Polski Juniorów                               | **21-23.10.2022**                                      |