import { Markdowns } from "@components/Markdown";
import { markdownsProps, sortByDate } from "@components/MarkdownStatic";

export default Markdowns("Archiwum", "posty");
export const getStaticProps = markdownsProps("_posts", sortByDate);
