import { Markdowns } from "@components/Markdown";
import { markdownsProps, sortByTitle } from "@components/MarkdownStatic";

export default Markdowns("Gdzie Gramy?", "rozgrywki");
export const getStaticProps = markdownsProps("_locations", sortByTitle);
