# Pliki do pobrania

- [Test kwalifikacyjny do kadry młodzików 2022](/files/2022Test.pdf)
- [Oświadczenie RODO Śląska Federacja Sportu Zawodnik Niepełnoletni](/files/rodoniepelnoletni.pdf)
- [Oświadczenie RODO Śląska Federacja Sportu Trener](/files/rodotrener.pdf)
- [Druki Śląskiej Federacji Sportu](https://www.federacja.net.pl/4,pl,pliki.html)
