import { sortByFilename } from "@components/MarkdownStatic";
import fs from "fs";
import path from "path";

const Home = ({ files }) => {
  return (
    <>
      <h1>ABC Brydża</h1>
      <ul>
        {files.map((post) => (
          <li>
            <h3>
              <a href={"/abc/" + post.filename} target="_blank">
                {post.slug}
              </a>
            </h3>
          </li>
        ))}
        <li>
          <a href="https://szkola.pzbs.pl/">Szkoła brydża PZBS</a>
        </li>
        <li>
          <a href="https://szkola.pzbs.pl/archiwum/">
            Stara Szkoła brydża PZBS
          </a>
        </li>
      </ul>
    </>
  );
};

export const getStaticProps = async () => {
  const files = fs
    .readdirSync(path.join("public/abc"))
    .map((filename) => ({
      filename,
      slug: filename.split(".")[0],
    }))
    .sort(sortByFilename);

  return {
    props: {
      files,
    },
  };
};

export default Home;
