import fs from "fs";
import path from "path";

const Home = ({ basicConventions, proConventions }) => {
  return (
    <>
      <h1>System STREFA</h1>
      {/* <h2>System dla dzieci:</h2> */}
      <h2>System dla młodzieży:</h2>
      <ul>
        <li>
          <h3>
            <a href="/system/naszsystem.pdf" target="_blank">
              Nasz System
            </a>
          </h3>
        </li>
      </ul>
      <h2>System profesjonalny:</h2>
      <ul>
        <li>
          <h3>
            <a href="/system/olimpijczykbbsystem.pdf" target="_blank">
              System Olimpijczyk Bielsko-Biała
            </a>
          </h3>
        </li>
      </ul>
      {/* <h2>Konwencje podstawowe:</h2> */}
      <h2>Konwencje profesjonalne:</h2>
      <ul>
        {proConventions.map((post) => (
          <li>
            <h3>
              <a href={"/system/proconvs/" + post.filename} target="_blank">
                {post.slug}
              </a>
            </h3>
          </li>
        ))}
      </ul>
    </>
  );
};

export const getStaticProps = async () => {
  // const basicConventions = fs
  //   .readdirSync(path.join("public/system/convs"))
  //   .map((filename) => {
  //     return {
  //       filename,
  //       slug: filename.split(".")[0].replace(/-/g, " "),
  //     };
  //   });

  const proConventions = fs
    .readdirSync(path.join("public/system/proconvs"))
    .map((filename) => {
      return {
        filename,
        slug: filename.split(".")[0].replace(/-/g, " "),
      };
    });

  return {
    props: {
      proConventions,
    },
  };
};

export default Home;
