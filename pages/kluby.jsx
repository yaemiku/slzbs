import { Markdowns } from "@components/Markdown";
import { markdownsProps, sortByTitle } from "@components/MarkdownStatic";

export default Markdowns("Kluby", "klub");
export const getStaticProps = markdownsProps("_clubs", sortByTitle);
