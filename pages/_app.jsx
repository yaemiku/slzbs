import "@styles/globals.css";
import Layout from "@components/Layout";
import { useEffect } from "react";

function Application({ Component, pageProps }) {
  useEffect(() => {
    if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  });

  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default Application;
