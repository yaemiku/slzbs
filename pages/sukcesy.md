# Sukcesy

1. Sukcesy 2021

   - 22 maja w Skawinie odbyły się eliminacje do Ogólnopolskiej Olimpiady Młodzieży w sportach halowych do finału w Tuszynie awansowało 11 par z klubu ( 10 z VLO) Artykuł na stronie V LO https://lo5.bielsko.pl/aktualnosci/piatka-brydzem-stoi
   - Brydżowa Europejska Liga Kobieca – artykuł na stronie VLO https://lo5.bielsko.pl/news/brydzowa-europejska-liga-kobieca/510

2. Sukcesy 2020

   - Mistrzostwa Polski Młodzików Starachowice 2020 – II miejsce w klasyfikacji klubów, I miejsce w klasyfikacji szkół, III miejsce w klasyfikacji województw https://wyniki.pzbs.pl/turnieje/2020/mpmlod/
   - Ogólnopolska Olimpiada Młodzieży w sportach halowych Lublin 2020 – I miejsce w klasyfikacji klubów i I miejsce w klasyfikacji województw https://wyniki.pzbs.pl/turnieje/2020/oom/chmak/
   - Drużynowe Mistrzostwa Polski Online U20 https://www.pzbs.pl/wyniki/mlodziez/2020/online/U20/
   - Artykuł na stronie V LO o innych zawodach https://www.lo5.bielsko.pl/news/druzynowe-mistrzostwa-polski-u-20-w-brydzu-online/384
   - III miejsce Jakuba Ledwonia w Mistrzostwach Polski Par U-15 http://www.bsbs.pl/wyniki/Styczen/MP15/congressscore.html
   - I miejsce Mateusza Bakalarskiego i Maximiliana Szemika w Mistrzostwach Polski Par U-20 http://www.bsbs.pl/wyniki/Styczen/MP20/congressscore.html
