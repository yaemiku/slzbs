# Dlaczego warto grać w brydża?

W brydżu liczy się nie sprawność rąk czy nóg, ale tylko głowy, czyli to co starzeje się najwolniej, a zgodnie z badaniami naukowymi brydż utrzymuje nasze umysły w młodości. Brydż się też nie nudzi, a wręcz przeciwnie – z każdym rozdaniem odkrywa się nowe problemy, których rozwiązanie jest niekończącym się źródłem przyjemności. Pytaniem nie powinno być dlaczego grać w brydża, tylko dlaczego nie!

W brydża nie powinien grać tylko ktoś, kto:

- nie czerpie przyjemności z samorozwoju,
- oczekuje, że z dnia na dzień zostanie mistrzem świata,
- nie toleruje gdy, jak w życiu, dobre decyzje nie zawsze prowadzą do dobrych rezultatów, mimo że konsekwentne unikanie złych decyzji na dystansie popłaca.

## [Warren Buffet i Bill Gates](https://www.youtube.com/watch?v=2d4KaiCKqjU)

## [Znane osoby grające w brydża](https://www.bridgebum.com/bridge_players.php)

## [Jak to się zaczyna?](https://www.youtube.com/watch?v=zkL9SBDFFIk)
