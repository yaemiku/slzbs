# Kontakt

## Adrian Bakalarz

Koordynator ds. młodzieży SLZBS  
bakadrian@interia.pl;  
698 48 44 49  
Odpowiedzialny za brydża w Bielsku-Białej  
<a href="http://bsbs.pl" target="_blank">Beskidzkie Stowarzyszenie Brydża Sportowego</a>

## Tomasz Kierepka

sbsblueclub@gmail.com;  
501 388 999  
Odpowiedzialny za brydża w Chorzowie i okolicy

## Jerzy Matura

jmatura@buks.slask.pl;  
507 122 884  
Odpowiedzialny za brydża w Bytomiu

## Dawid Ochman

dawidochman@vp.pl;  
691 186 353  
Odpowiedzialny za brydża w Pszczynie

## Mikołaj Kubiczek

me@yaemiku.dev - [klucz PGP](https://yaemiku.dev/public.asc)  
[yaemiku.dev](https://yaemiku.dev)  
Twórca i administrator strony
