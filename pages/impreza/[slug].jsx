import { Markdown } from "@components/Markdown";
import { markdownProps, markdownPaths } from "@components/MarkdownStatic";

const dir = "_events";

export default Markdown;
export const getStaticProps = markdownProps(dir);
export const getStaticPaths = markdownPaths(dir);
