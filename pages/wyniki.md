# Wyniki

Mistrzostwa Polski Dzieci  
https://wyniki.pzbs.pl/2022/mp_u12/pary/

Ogólnopolska Olimpiada Młodzieży 2022  
http://czaja.pzbs.pl/wyniki/2022/oom/

Eliminacje do Ogólnopolskiej Olimpiady Młodzieży 2022  
https://mzbs.pl/files/2021/wyniki/zs/eoom_dz/
https://mzbs.pl/files/2021/wyniki/zs/eoom_ch/

### Turnieje cykliczne

Wtorki 21.10 turnieje indywidualne

Czwartki 20.00 turnieje House of Bridge  
https://play.houseofbridge.com/tournaments

Piątki 16.00 turnieje UTW + Młodzież  
http://www.bsbs.pl/index.php/wyniki-turniejow
