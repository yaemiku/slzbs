module.exports = {
  mode: "jit",
  content: [
    "./components/**/*.{html,js,jsx}",
    "./pages/**/*.{html,js,jsx}",
    "./_events/**/*.md",
  ],
  darkMode: "class",
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/typography")],
};
